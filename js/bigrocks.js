// written by bigrocks89@outlook.com 2022_03_27_8_50_AM


//next page button 
$('body').on('click','#next_page', function(event){
    event.preventDefault();
    var page_num = $(this).closest('li').siblings('.active').find('a').text();
    page_num = parseInt(page_num);
    page_num++;
    var ac_sib = $(this).closest('li').siblings('.active');
    ac_sib.next().addClass('active');
    ac_sib.removeClass('active');
    console.log(page_num);

});

// prev page button
$('body').on('click','#prev_page', function(event){
    event.preventDefault();
    var page_num = $(this).closest('li').siblings('.active').find('a').text();
    page_num = parseInt(page_num);
    page_num--;
    var ac_sib = $(this).closest('li').siblings('.active');
    ac_sib.prev().addClass('active');
    ac_sib.removeClass('active');
    console.log(page_num);

});

// show search panel
$('.Show').on('click', function(){
    $(this).css('display','none');
    $('#target').css('display','block');
});

// hide search panel
$('.Hide').on('click', function(){
    $('.Show').css('display','inline-block');
    $('#target').css('display','none');
});