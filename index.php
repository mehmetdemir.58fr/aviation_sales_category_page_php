<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>



	<!-- site header -->
		<div class="jet-header">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<nav class="navbar navbar-expand-lg navbar-light px-0">
					  	<a class="navbar-brand" href="#"><img src="./images/jet-logo.png"></a>
						  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						    <span class="navbar-toggler-icon"></span>
						  </button>

						  <div class="collapse navbar-collapse " id="navbarSupportedContent">
						    <ul class="navbar-nav ml-auto">
						      <li class="nav-item active">
						        <a class="nav-link" href="#">Home</a>
						      </li>
						      <li class="nav-item">
						        <a class="nav-link" href="#">Buy Aircraft</a>
						      </li>
						      <li class="nav-item">
						        <a class="nav-link" href="#">Sell Aircraft</a>
						      </li>
						      <li class="nav-item">
						        <a class="nav-link login_btn" href="#">Login</a>
						      </li>
						      <li class="nav-item">
						        	<a class="nav-link register_btn" href="#">Register</a>
						      </li>
						    </ul>
						  </div>
						</nav>
					</div>
				</div>
			</div>
		</div>
	<!-- site header -->


	<!-- plane details popup  -->
	<div class="plane-details" style="display: none;">
		<div class="row no-gutters">
			<div class="col-md-12">
				<p class="font-weight-bold">Cessna Citation CJ2</p>
			</div>
			<div class="col-md-7"> 
				<div class="details-view-img"> 
					<img src="images/bplane.png" class="img-fluid"> 
				</div>
			</div>
			<div class="col-md-5 pl-0 pl-sm-3 mt-3 mt-sm-0">
				<div class="row">
					<div class="col-6 pb-3 pr-2"><img src="images/mini.png" class="img-fluid"></div>
					<div class="col-6 pb-3 pl-2"><img src="images/mini.png" class="img-fluid"></div>
				</div>
				<div class="row">
					<div class="col-6 pr-2"><img src="images/mini.png" class="img-fluid"></div>
					<div class="col-6 pl-2"><img src="images/mini.png" class="img-fluid"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="jet_price py-2">$ 15164658</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="jet-specification">
					<ul>
						<li><span>Make : </span></li>
						<li><span>Price : </span></li>
						<li><span>Pax : </span></li>
						<li><span>Height (ft) : </span></li>
						<li><span>Length (ft) : </span></li>
						<li><span>Length (ft) : </span></li>
						<li><span>Range (nm) : </span></li>
						<li><span>Qty Mode  : </span></li>
						<li><span>Avg Price in Aftermarket : </span></li>
						<li><span>Gross Weight (ibs) : </span></li>
						<li><span>Standard Fuel (L) : </span></li>
						<li><span>75% Cruise (kts) : </span></li>
						<li><span>rRate of Climb (ft/min) : </span></li>
						<li><span>Landing (ft)  </span></li>
						<li><span>Landing 50ft Obstacle (ft) : </span></li>
						<li><span># of Engines : </span></li>
						<li><span>Engine Model :</span></li>
					</ul>
				</div>
			</div>
			<div class="col-md-6">
				<div class="jet-specification">
					<ul>
						<li><span>Model : </span></li>
						<li><span>Production :</span></li>
						<li><span>Volume (cu ft) : </span></li>
						<li><span>Width (ft) : </span></li>
						<li><span>Wingspan (ft) : </span></li>
						<li><span>Height (ft) : </span></li>
						<li><span>Fuel Burn (gph) : </span></li>
						<li><span>Qty in Aftermarket : </span></li>
						<li><span>Empty Weight (Ibs) : </span></li>
						<li><span>Max Takeoff Weight (Ibs) : </span></li>
						<li><span>Max Fuel (L) : </span></li>
						<li><span>Service Ceiling (ft) : </span></li>
						<li><span>Takeoff (ft) : </span></li>
						<li><span>Takeoff 50ft Obstacle (ft) :  </span></li>
						<li><span>TBO : </span></li>
						<li><span>Engine Make : </span></li>
						<li><span>Engine Thrust (Ibf) :</span></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row align-items-center mt-3">
				<div class="col-md-6 col-12">
					<div class="mini-map d-flex justify-content-between align-items-center">
						<img src="images/mini-map.png">
						<div class="pl-2">
							<h5>Range 694 nmi</h5>
							<input type="text" class="form-control form-control-sm" placeholder="Departure Airport">
						</div>
					</div>
				</div>
				<div class="col-md-6 text-right col-12 mt-3 mt-sm-0">
					<a href="#" class="common-btn d-block d-sm-inline-block P-1 ">Buy / Sell </a>
				</div>
		</div>
	</div>
<!-- plane details popup  -->


	<!-- jet search panel -->
		<section class="jet_search_panel pt-0  ">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-right filter-btn">
						<button type="button" class="btn Show common-btn">Filter Results <i class="fa fa-angle-down"></i> </button>
					</div>
				</div>
				<div id="target" class="pb-5 pt-3">
				<div class="row">
					<div class="col mb-col">
						<div class="form-group">
							<label>Sort by</label>
							<div class="position-relative cstm-select">
							<select class="form-control ">
								<option>Weight - High to Low</option>
								<option>Weight - Low to High</option>
							</select>
							</div>
						</div>
					</div>
					<div class="col mb-col">
						<div class="form-group">
							<label>Type</label>
							<div class="position-relative cstm-select">
							<select class="form-control">
								<option>Jet</option>
								<option>Superjet</option>
							</select>
						</div>
						</div>
					</div>
					<div class="col mb-col">
						<div class="form-group">
							<label>Engine</label>
							<div class="position-relative cstm-select">
							<select class="form-control">
								<option>Single</option>
								<option>Double</option>
							</select>
						</div>
						</div>
					</div>
					<div class="col mb-col">
						<div class="form-group">
							<label>Units</label>
							<div class="position-relative cstm-select">
							<select class="form-control">
								<option>lbs</option>
								<option>lbs</option>
							</select>
						</div>
						</div>
					</div>
					<div class="col mb-col">
						<div class="form-group">
							<label>Weight</label>
							<div class="position-relative cstm-select">
							<select class="form-control">
								<option>Useful Load</option>
								<option>Useful Load</option>
							</select>
						</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col mb-col pb-5">
						<div class="subject text-center w-100">Price</div>
						<div class="ratio d-flex justify-content-between pb-2">
							<span>$5000</span>
							<span>$100000</span>
						</div>
						<div class="dynamic-value position-relative">
							<div class="slide-img">
								<div class="slide-value">
									<img src="images/1-plane.png">
									<span>$20000</span>
								</div>
								<div class="slide-value">
									<img src="images/2-plane.png">
									<span>$20000</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col mb-col pb-5">
						<div class="subject text-center w-100">Production Years</div>
						<div class="ratio d-flex justify-content-between pb-2">
							<span>2001</span>
							<span>2022span>
						</div>
						<div class="dynamic-value position-relative">
							<div class="slide-img">
								<div class="slide-value">
									<img src="images/1-plane.png">
									<span>2001</span>
								</div>
								<div class="slide-value">
									<img src="images/2-plane.png">
									<span>2021</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col mb-col pb-5">
						<div class="subject text-center w-100">Pax</div>
						<div class="ratio d-flex justify-content-between pb-2">
							<span>0</span>
							<span>200</span>
						</div>
						<div class="dynamic-value position-relative">
							<div class="slide-img">
								<div class="slide-value">
									<img src="images/1-plane.png">
								</div>
								<div class="slide-value">
									<img src="images/2-plane.png">
									<span>30</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col mb-col pb-5">
						<div class="subject text-center w-100">Height</div>
						<div class="ratio d-flex justify-content-between pb-2">
							<span>0</span>
							<span>50</span>
						</div>
						<div class="dynamic-value position-relative">
							<div class="slide-img">
								<div class="slide-value">
									<img src="images/1-plane.png">
									<!-- <span>$20000</span> -->
								</div>
								<div class="slide-value">
									<img src="images/2-plane.png">
									<span>30</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col mb-col pb-5">
						<div class="subject text-center w-100">Volume</div>
						<div class="ratio d-flex justify-content-between pb-2">
							<span>S0</span>
							<span>S100</span>
						</div>
						<div class="dynamic-value position-relative">
							<div class="slide-img">
								<div class="slide-value">
									<img src="images/1-plane.png">
									<!-- <span>$20000</span> -->
								</div>
								<div class="slide-value">
									<img src="images/2-plane.png">
									<span>15C</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-9 offset-md-3">
						<div class="form-group mb-0">
					      <input type="checkbox" id="com01">
					      <label for="com01">Show Only In Production</label>
					    </div>
					</div>
				</div>
				<div class="row">
					<div class="col mb-col pb-5">
						<div class="subject text-center w-100">Range</div>
						<div class="ratio d-flex justify-content-between pb-2">
							<span>0</span>
							<span>800</span>
						</div>
						<div class="dynamic-value position-relative">
							<div class="slide-img">
								<div class="slide-value">
									<img src="images/1-plane.png">
									<!-- <span>$20000</span> -->
								</div>
								<div class="slide-value">
									<img src="images/2-plane.png">
									<span>30</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col mb-col pb-5">
						<div class="subject text-center w-100">Fuel Burn 1</div>
						<div class="ratio d-flex justify-content-between pb-2">
							<span>0</span>
							<span>200</span>
						</div>
						<div class="dynamic-value position-relative">
							<div class="slide-img">
								<div class="slide-value">
									<img src="images/1-plane.png">
									<!-- <span>$20000</span> -->
								</div>
								<div class="slide-value">
									<img src="images/2-plane.png">
									<span>200</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col mb-col pb-5">
						<div class="subject text-center w-100">Speed</div>
						<div class="ratio d-flex justify-content-between pb-2">
							<span>10kts</span>
							<span>100kts</span>
						</div>
						<div class="dynamic-value position-relative">
							<div class="slide-img">
								<div class="slide-value">
									<img src="images/1-plane.png">
									<!-- <span>$20000</span> -->
								</div>
								<div class="slide-value">
									<img src="images/2-plane.png">
									<span>15km/h</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col mb-col pb-5">
						<div class="subject text-center w-100">Takeoff</div>
						<div class="ratio d-flex justify-content-between pb-2">
							<span>0</span>
							<span>2000</span>
						</div>
						<div class="dynamic-value position-relative">
							<div class="slide-img">
								<div class="slide-value">
									<img src="images/1-plane.png">
									<!-- <span>$20000</span> -->
								</div>
								<div class="slide-value">
									<img src="images/2-plane.png">
									<span>2000</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col mb-col pb-5">
						<div class="subject text-center w-100">Landing</div>
						<div class="ratio d-flex justify-content-between pb-2">
							<span>0</span>
							<span>2000</span>
						</div>
						<div class="dynamic-value position-relative">
							<div class="slide-img">
								<div class="slide-value">
									<img src="images/1-plane.png">
									<!-- <span>$20000</span> -->
								</div>
								<div class="slide-value">
									<img src="images/2-plane.png">
									<span>2000</span>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row pt-4">
					<div class="col-md-7">
						<div class="form-group mb-0  filter-search position-relative">
							<input type="text" class="form-control" placeholder="Search" name="">
							<span><i class="fa fa-search"></i>  </span>
						</div>
					</div>
					<div class="col-md-5 text-center text-md-right pt-3 pt-sm-0">
						<a href="#" class="common-btn Hide">close</a>
					</div>
				</div>
			</div>
			</div>
		</section>

	<!-- jet search panel -->


<!-- jet view section -->
	<section class="jet_view mt-5">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="view-top pb-3 d-flex justify-content-between align-items-center" >
					
							<nav class="view-tab">
									
							  <div class="nav nav-tabs" id="nav-tab" role="tablist">
							  	<p class="pr-2">View</p>
							    <a class="nav-item nav-link active" id="list-view-tab" data-toggle="tab" href="#list-view" role="tab" aria-controls="list-view" aria-selected="true"><i class="fa fa-list-ul"></i> </a>
							    <a class="nav-item nav-link" id="box-view-tab" data-toggle="tab" href="#box-view" role="tab" aria-controls="box-view" aria-selected="false"><i class=" fa fa-th"></i></a>
							  </div>
							</nav>
						<div class="compare_aircraft">Compare Aircraft</div>
					</div>
					

					<div class="tab-content" id="nav-tabContent">
					  <div class="tab-pane fade show active" id="list-view" role="tabpanel" aria-labelledby="list-view-tab">
					  	<!-- list view single jet details -->
						  <?php

							  $iterator = 0;
							  $list_pagination = 5;
							  $display_css = 'inline-block';

									try {
									    $dbh = new PDO('mysql:host=localhost;dbname=airplanedatabase', 'root', '');
										
									    foreach($dbh->query('SELECT * from sheet') as $row) {
									        $iterator++;
											if($list_pagination == $iterator){
												break;
											}
											echo '<div class="card p-1 d-inline-block w-100 mb-2">
					  								<div class="jet-selfie-list">
					  									<img src="images/plane.png" class="img-fluid">
					  								</div>
					  								<div class="jet-details pl-2 pl-sm-4 py-0 py-sm-2">
					  									<div class="details_left">
					  										<div class="jet_tittle">'.$row['aircraft'].'</div>
												  			<div class="jet-specify pb-0 pb-sm-2">
												  				<ul>
												  					<li>'.$row['prod_y_start'].'</li>
												  					<li>'.$row['cruise_75'].'</li>
												  					<li>Range '.$row['range'].' nmi</li>
												  					<li>'.$row['cabin_typical_pax'].' passengers</li>
												  				</ul>
												  			</div>
												  			<div class="jet-compare">
												  				<div class="form-group mb-0">
															      <input type="checkbox" id="com1">
															      <label for="com1">Compare</label>
															    </div>
												  			</div>
												  			<div class="jet_price">
												  				$ '.$row['price'].'
												  			</div>
												  		</div>
												  		<div class="details_right">
												  			<a href="#" class="common-btn Show"> Buy /  Sell</a>
												  		</div>
					  								</div>
					  							</div>';
									    }
									    $dbh = null;
									} catch (PDOException $e) {
									    print "Error!: " . $e->getMessage() . "<br/>";
									    die();
									}
								?>
					  	
					  	<!-- list view single jet details -->

					  </div>
					  <div class="tab-pane fade" id="box-view" role="tabpanel" aria-labelledby="box-view-tab">
					  	
					  		<div class="row">
							  <?php

							  $iterator = 0;
							  $box_pagination = 9;
							  $display_css = 'inline-block';

									try {
									    $dbh = new PDO('mysql:host=localhost;dbname=airplanedatabase', 'root', '');
										$results = $dbh->query('SELECT * from sheet');
									    foreach($results as $row) {
									        $iterator++;
											if($box_pagination == $iterator){
												break;
											}
											echo '<div class="col-md-6 col-lg-3 col-12 col-sm-6 mb-4">
							  						<div class="card jet-box">
							  							<div class="jet-selfie-view	">
							  							<img src="images/bplane.png" class="img-fluid">
							  						</div>
							  						<div class="px-3 py-3">
							  							<div class="jet_tittle">'.$row['aircraft'].'</div>
							  						<div class="jet-specify py-2">
								  						<ul>
								  							<li>'.$row['prod_y_start'].'</li>
								  							<li>'.$row['cruise_75'].'</li>
								  							<li>Range '.$row['range'].' nmi</li>
								  							<li>'.$row['cabin_typical_pax'].' passengers</li>
								  						</ul>
								  					</div>
								  					<div class="d-flex justify-content-between align-items-center pb-2">
								  						<div class="jet_price">
									  						$ '.$row['price'].'
									  					</div>
								  						<div class="jet-compare">
									  						<div class="form-group mb-0">
														      <input type="checkbox" id="com6">
														      <label for="com6">Compare</label>
														    </div>
									  					</div>

								  					</div>
								  					<a href="#" class="common-btn d-block"> Buy /  Sell</a>
							  						</div>

							  						</div>

							  					</div>';
									    }
									    $dbh = null;
									} catch (PDOException $e) {
									    print "Error!: " . $e->getMessage() . "<br/>";
									    die();
									}
								?> 

					  		</div>
					  
					  </div>
					</div>
				</div>
				<div class="col-md-12 bottom-pagination py-4">
					<p class="mb-0">Showing 1-<?php echo $list_pagination; ?> of <?php echo $results->rowCount(); ?></p>
					<div class="page-pagination">
						<nav aria-label="Page navigation example">
						  <ul class="pagination">
						    <li class="page-item disabled" id="prev_page">
						      <a class="page-link" href="#" tabindex="-1">Previous</a>
						    </li>
						    <li class="page-item active"><a class="page-link" href="#">1</a></li>
						    <li class="page-item"><a class="page-link" href="#">2</a></li>
						    <li class="page-item"><a class="page-link" href="#">3</a></li>
						    <li class="page-item">
						      <a class="page-link" href="#" id="next_page">Next</a>
						    </li>
						  </ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</section>

<!-- jet view section -->


<!-- footer section -->
<section class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3 footer-logo">
				<img src="images/jet-logo.png" class="img-fluid">
				<p class="ftr-details">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quis, repellendus?</p>
			</div>
			<div class="col-md-3">
				<ul class="footer-link">
					<li><a href="#">Home</a></li>
					<li><a href="#">Buy Aircraft</a></li>
					<li><a href="#">Sell Aircraft</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<ul class="footer-link">
					<li><a href="#">Terms & Conditions</a></li>
					<li><a href="#">Privacy & Cookies</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<p>Follow us on</p>
				<ul class="social-link">
					<li><a href="#"><i class="fa fa-instagram"></i></a></li>
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="footer-bottom">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 text-center">
				&copy; 2002-2022 JetBuyerguide Ltd. All Rights Reserved
			</div>
		</div>
	</div>
</section>
<!-- footer section -->




	

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" ></script>
<script src="./js/bigrocks.js">
<script>

    $('.Show').click(function() {
      $('#target').show(500);
      $('.Show').hide(0);
      $('.Hide').show(0);
  });
  $('.Hide').click(function() {
      $('#target').hide(500);
      $('.Show').show(0);
      $('.Hide').hide(0);
  });
</script>
</body>
</html>